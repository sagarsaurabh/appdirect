# README #

## Appdirect Api Integration ##

**As of now following integrations are supported.**

1. Subscription Create
2. Subscription Cancel
3. Subscription Change

* This is a Spring Boot application for managing user budget (Very limited functionality implemented).

* UI has not been added yet. Only Rest implementation is done.

* Can be run by cloning the repository and running the same as spring boot application.

* Database used is Postgresql.
* DB script is present at /budgetapp/src/main/resources/db/

**Oauth key and secret can be modified in security.properties file.**
