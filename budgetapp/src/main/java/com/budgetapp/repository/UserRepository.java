package com.budgetapp.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.budgetapp.model.Users;

public interface UserRepository extends JpaRepository<Users, Long> {
       public Users getUsersByAppDirectUserId(String appDirectUserId);
       
      /* @Query("select a from Users u inner join u.accounts a where u.appDirectUserId = :appdirectUserId and a.status='ACTIVE'")
       public List<Account> getActiveAccountsForUser(String appdirectUserId);*/
       
}
