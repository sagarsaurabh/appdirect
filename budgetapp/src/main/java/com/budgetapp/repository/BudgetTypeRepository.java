package com.budgetapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.budgetapp.model.BudgetType;

public interface BudgetTypeRepository extends JpaRepository<BudgetType, Long>{
   public BudgetType getByBudgetName(String budgetName);
}
