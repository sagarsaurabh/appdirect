package com.budgetapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.budgetapp.model.UserBudget;

public interface UserBudgetRepositoty extends JpaRepository<UserBudget, Long> {
       
	@Query("select ub from UserBudget ub join ub.user u where u.appDirectUserId= :userId")
	public List<UserBudget> getBudgetsByUserId(@Param("userId") String userId);
	
	/*@Query("select b from UserBudget ub join ub.budget b where b.budgetName= :budgetName")
	public BudgetType getBudgetType(@Param("budgetName") String budgetName);*/
	
}
