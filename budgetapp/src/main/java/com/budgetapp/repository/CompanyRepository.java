package com.budgetapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.budgetapp.model.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> {

	public Company getCompanyByUuid(String uuid);
	
}
