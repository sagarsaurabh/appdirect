package com.budgetapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.budgetapp.model.Subscription;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {

	public Subscription getSubscriptionByEdition(String edition);
	
}
