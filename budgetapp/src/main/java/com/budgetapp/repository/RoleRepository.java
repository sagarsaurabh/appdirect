package com.budgetapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.budgetapp.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{
      public Role getRoleByRoleName(String roleName);
}
