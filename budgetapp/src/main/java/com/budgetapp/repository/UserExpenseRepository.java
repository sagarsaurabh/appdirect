package com.budgetapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.budgetapp.model.UserExpense;

public interface UserExpenseRepository extends JpaRepository<UserExpense, Long> {
	@Query("select ue from UserExpense ue join ue.user u where u.appDirectUserId=:userId")
	public List<UserExpense> getUserExpenses(@Param("userId") String userId);
    
	@Query("select ue from UserExpense ue join ue.user u join ue.budgetType bt where u.appDirectUserId=:userId and bt.budgetName=:budgetType")
	public List<UserExpense> getUserExpenseOfType(@Param("budgetType") String budgetType, @Param("userId") String userId);
	
}
