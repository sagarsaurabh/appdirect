package com.budgetapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.budgetapp.model.Account;



public interface AccountRepository extends JpaRepository<Account, Long> {
	public Account getAccountByAccountIdentifier(String accountIdentifier);
	
	
}
