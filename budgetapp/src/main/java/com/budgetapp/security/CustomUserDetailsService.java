package com.budgetapp.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.openid.OpenIDAuthenticationToken;

import com.budgetapp.dto.AccountStatus;
import com.budgetapp.exception.BATechnicalException;
import com.budgetapp.model.Account;
import com.budgetapp.model.Users;
import com.budgetapp.repository.UserRepository;



public class CustomUserDetailsService implements AuthenticationUserDetailsService<OpenIDAuthenticationToken> {
  
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomUserDetailsService.class);
	
	private UserRepository userRepository;
	
	public CustomUserDetailsService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}



	public UserDetails loadUserDetails(OpenIDAuthenticationToken token)
            throws UsernameNotFoundException {
    	String appdirectUserId = token.getName().substring(token.getName().lastIndexOf("/") + 1);
        return loadUserByUsername(appdirectUserId);
    }
	
    
    
    public UserDetails loadUserByUsername(String appdirectUserId) throws UsernameNotFoundException {
        try {
            Users user = userRepository.getUsersByAppDirectUserId(appdirectUserId);
            if (user == null) {
                LOGGER.debug("user not found with the provided appdirectUserId " + appdirectUserId);
                return null;
            }
            if(!isUserHasActiveAccount(user)) {
            	LOGGER.error("User " + appdirectUserId + " is not registerd with any active account");
            	throw new BATechnicalException("User " + appdirectUserId + " is not registerd with any active account");
            }
            LOGGER.debug(" user with appdirectUserId " + user.toString());
            return new org.springframework.security.core.userdetails.User(user.getAppDirectUserId(), "" , AuthorityUtils.createAuthorityList("ROLE_USER"));
        }
        catch (Exception e){
            throw new UsernameNotFoundException("User not found");
        }
    }

    private boolean isUserHasActiveAccount(Users user) {
		for(Account account : user.getAccounts()) {
			if(account.getStatus().equals(AccountStatus.ACTIVE)) {
				return true;
			}
		}
		return false;
	}

	
}