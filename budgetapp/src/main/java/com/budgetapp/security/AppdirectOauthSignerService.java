package com.budgetapp.security;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.basic.DefaultOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

@Service
@PropertySource("classpath:security.properties")
public class AppdirectOauthSignerService {
    
	@Value("${appdirect.consumer.key}")
    private String consumerKey;
	
	@Value("${appdirect.consumer.secret}")
	private String consumerSecret;
	
   private static final Logger LOGGER = LoggerFactory.getLogger(AppdirectOauthSignerService.class);
	
	public <T> T signAndGetResponseFromAppDirect(String subscriptionUrl, Class<T> mappingObject) throws OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException, IOException, URISyntaxException {
				
			OAuthConsumer consumer = new DefaultOAuthConsumer(consumerKey, consumerSecret);
			String signedUrl = consumer.sign(subscriptionUrl);
            
			HttpClient client = HttpClientBuilder.create().build();
	        HttpGet getRequest = new HttpGet(signedUrl);
	        LOGGER.info("signed url to be fetched " + signedUrl);
	        getRequest.setHeader("Accept", "application/json");
	        HttpResponse response = client.execute(getRequest);
	        HttpEntity entity = response.getEntity();
       	    InputStream is = entity.getContent();
	         ObjectMapper mapper = new ObjectMapper();

           return mapper.readValue(is, mappingObject);
	       	
		
	}
	
	
}
