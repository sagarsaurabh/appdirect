package com.budgetapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionChangeEvent {

	private String type;
	   private Marketplace marketPlace;
	   private User user;
	   private PayLoad payload;
	   
	@JsonProperty("payload")	
	public PayLoad getPayload() {
		return payload;
	}

	public void setPayload(PayLoad payload) {
		this.payload = payload;
	}

	@JsonProperty("creator")	
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@JsonProperty("type")
		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		@JsonProperty("marketplace")
		public Marketplace getMarketPlace() {
			return marketPlace;
		}

		public void setMarketPlace(Marketplace marketPlace) {
			this.marketPlace = marketPlace;
		}
		
		
		

	
}
