package com.budgetapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionOrder {

	@JsonProperty(value="editionCode")
	private String editionCode;
	
	@JsonProperty(value="pricingDuration")
	private String pricingDuration;
	
	@JsonProperty(value="item")
	private SubscriptionItem item;
	
	
	public String getEditionCode() {
		return editionCode;
	}
	public void setEditionCode(String editionCode) {
		this.editionCode = editionCode;
	}
	public String getPricingDuration() {
		return pricingDuration;
	}
	public void setPricingDuration(String pricingDuration) {
		this.pricingDuration = pricingDuration;
	}
	public SubscriptionItem getItem() {
		return item;
	}
	public void setItem(SubscriptionItem item) {
		this.item = item;
	}
	
	
}
