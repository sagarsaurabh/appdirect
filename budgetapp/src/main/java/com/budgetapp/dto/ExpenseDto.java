package com.budgetapp.dto;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExpenseDto {

	@JsonProperty("amount")
      private BigDecimal amount;
	  @JsonProperty("budgetType")
	  private String budgetTypeName;
      @JsonProperty("userId")	
	  private String userId;
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getBudgetTypeName() {
		return budgetTypeName;
	}
	public void setBudgetTypeName(String budgetTypeName) {
		this.budgetTypeName = budgetTypeName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
      
      
	
}
