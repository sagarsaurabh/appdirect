package com.budgetapp.dto;

public enum AccountStatus {
	REACTIVATED, DEACTIVATED, ACTIVE, CLOSED, UPCOMING_INVOICE,FREE_TRIAL
}
