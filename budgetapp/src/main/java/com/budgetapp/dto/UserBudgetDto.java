package com.budgetapp.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UserBudgetDto {
	
	@JsonProperty("userId")
	private String appdirectUserId;
	@JsonProperty("budgetType")
	private String budgetType;
	@JsonProperty("from")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	private Date fromDate;
	@JsonProperty("amount")
	private BigDecimal budgetAmount;
	
	public String getAppdirectUserId() {
		return appdirectUserId;
	}
	public void setAppdirectUserId(String appdirectUserId) {
		this.appdirectUserId = appdirectUserId;
	}
	public String getBudgetType() {
		return budgetType;
	}
	public void setBudgetType(String budgetType) {
		this.budgetType = budgetType;
	}
	
	public BigDecimal getBudgetAmount() {
		return budgetAmount;
	}
	public void setBudgetAmount(BigDecimal budgetAmount) {
		this.budgetAmount = budgetAmount;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	
}
