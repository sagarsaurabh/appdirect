package com.budgetapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PayLoad {

	@JsonProperty(value="company")
	private Company company;
    
    @JsonProperty(value="order")
    private SubscriptionOrder subscriptionOrder;
    
    @JsonProperty(value="account")
    private Account account;
    
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public SubscriptionOrder getSubscriptionOrder() {
		return subscriptionOrder;
	}

	public void setSubscriptionOrder(SubscriptionOrder subscriptionOrder) {
		this.subscriptionOrder = subscriptionOrder;
	}
    
}
