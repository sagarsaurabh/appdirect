package com.budgetapp.dto;

import com.budgetapp.reports.ReportType;

public class ReportDto {
    private ReportType reportType;

	public ReportType getReportType() {
		return reportType;
	}

	public void setReportType(ReportType reportType) {
		this.reportType = reportType;
	} 
    
    
}
