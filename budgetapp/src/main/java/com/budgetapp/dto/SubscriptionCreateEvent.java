package com.budgetapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscriptionCreateEvent {

	@JsonProperty(value="type")
	private String type;

    @JsonProperty(value="creator")
	private User user;
    
    @JsonProperty(value="payload")
    private PayLoad payload;
    
    @JsonProperty(value="marketplace")
	private Marketplace marketplace;
    
    
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public PayLoad getPayload() {
		return payload;
	}
	public void setPayload(PayLoad payload) {
		this.payload = payload;
	}
	public Marketplace getMarketplace() {
		return marketplace;
	}
	public void setMarketplace(Marketplace marketplace) {
		this.marketplace = marketplace;
	}
	
	
	
}
