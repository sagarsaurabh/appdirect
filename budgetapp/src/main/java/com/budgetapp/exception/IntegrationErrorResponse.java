package com.budgetapp.exception;

import com.budgetapp.respose.integration.IntegrationResponse;

public class IntegrationErrorResponse extends IntegrationResponse {
      private int errorCode;

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
      
}
