package com.budgetapp.exception;

public class BATechnicalException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public BATechnicalException(String errorMessage) {
		super(errorMessage);
	}

	
}
