package com.budgetapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budgetapp.model.Account;
import com.budgetapp.repository.AccountRepository;

@Service
public class AccountServiceImpl implements AccountService{

	@Autowired
	private AccountRepository accountRepository;
	
	@Override
	public Account saveAccount(Account account) {
		
		return accountRepository.save(account);
	}

	@Override
	public Account getAccountByAccountIdentifier(String accountIdentifier) {
		
		return accountRepository.getAccountByAccountIdentifier(accountIdentifier);
	}

}
