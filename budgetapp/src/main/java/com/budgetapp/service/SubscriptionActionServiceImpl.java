package com.budgetapp.service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.UUID;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budgetapp.dto.AccountStatus;
import com.budgetapp.dto.SubscriptionCancelEvent;
import com.budgetapp.dto.SubscriptionChangeEvent;
import com.budgetapp.dto.SubscriptionCreateEvent;
import com.budgetapp.exception.BATechnicalException;
import com.budgetapp.model.Account;
import com.budgetapp.model.Company;
import com.budgetapp.model.Subscription;
import com.budgetapp.model.Users;
import com.budgetapp.respose.integration.SubscriptionCancelIntegrationResponse;
import com.budgetapp.respose.integration.SubscriptionChangeIntegrationResponse;
import com.budgetapp.respose.integration.SubscriptionCreateIntegrationResponse;
import com.budgetapp.security.AppdirectOauthSignerService;

import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

@Service
public class SubscriptionActionServiceImpl implements SubscriptionActionService {

	@Autowired
	private UserService userService;

	@Autowired
	private CompanyService companyService;

	/*
	 * @Autowired private RoleService roleService;
	 */

	@Autowired
	private AccountService accountService;

	@Autowired
	private SubscriptionService subscriptionService;

	@Autowired
	private AppdirectOauthSignerService oauthSignerService;

	private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionActionServiceImpl.class);

	@Override
	@Transactional
	public SubscriptionCreateIntegrationResponse createSubscription(String url) throws BATechnicalException {
		SubscriptionCreateEvent subscriptionCreateEvent = null;
		try {
			subscriptionCreateEvent = oauthSignerService.signAndGetResponseFromAppDirect(url,
					SubscriptionCreateEvent.class);
		} catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException
				| IOException | URISyntaxException e) {
			LOGGER.error("Error sign and get cancel subscription event URL");
			throw new BATechnicalException("Error sign and get cancel subscription event URL");
		}

		return buildSubscriptionCreateEventResponse(subscriptionCreateEvent);

	}

	@Override
	@Transactional
	public SubscriptionCancelIntegrationResponse cancelSubscription(String url) throws BATechnicalException {
		SubscriptionCancelEvent subscriptionCancelEvent = null;
		try {
			subscriptionCancelEvent = oauthSignerService.signAndGetResponseFromAppDirect(url,
					SubscriptionCancelEvent.class);
		} catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException
				| IOException | URISyntaxException e) {

			throw new BATechnicalException("Error sign and get cancel subscription event URL");
		}

		Account account = accountService.getAccountByAccountIdentifier(
				subscriptionCancelEvent.getPayload().getAccount().getAccountIdentifier());
		if (account == null) {
			throw new BATechnicalException("Account not found");
		}
		account.setStatus(AccountStatus.DEACTIVATED);
		accountService.saveAccount(account);

		SubscriptionCancelIntegrationResponse response = new SubscriptionCancelIntegrationResponse();
		response.setIs_success(true);
		response.setMessage("Successfully cancelled the subscription !!");
		return response;
	}

	@Override
	@Transactional
	public SubscriptionChangeIntegrationResponse changeSubscription(String url) throws BATechnicalException {

		SubscriptionChangeEvent subscriptionChangeEvent = null;
		try {
			subscriptionChangeEvent = oauthSignerService.signAndGetResponseFromAppDirect(url,
					SubscriptionChangeEvent.class);
		} catch (OAuthMessageSignerException | OAuthExpectationFailedException | OAuthCommunicationException
				| IOException | URISyntaxException e) {

			e.printStackTrace();
		}

		Account account = accountService.getAccountByAccountIdentifier(
				subscriptionChangeEvent.getPayload().getAccount().getAccountIdentifier());
		if (account != null && account.getStatus() != null) {
			Subscription subscription = subscriptionService.getSubscriptionByEdition(
					subscriptionChangeEvent.getPayload().getSubscriptionOrder().getEditionCode());
			if (subscription == null) {
				subscription = new Subscription();
				subscription.setEdition(subscriptionChangeEvent.getPayload().getSubscriptionOrder().getEditionCode());
				subscription.setPricingDuration(
						subscriptionChangeEvent.getPayload().getSubscriptionOrder().getPricingDuration());
			}
			account.setSubscription(subscription);
			accountService.saveAccount(account);
		} else {
			throw new BATechnicalException("No Account exists to change the subscription");
		}
		SubscriptionChangeIntegrationResponse response = new SubscriptionChangeIntegrationResponse();
		response.setIs_success(true);
		response.setMessage("Subscription changed successfully");
		return response;
	}

	private Users createNewUserWithCompany(SubscriptionCreateEvent subscriptionCreateEvent) {

		Company company = companyService.getCompanyByUuId(subscriptionCreateEvent.getPayload().getCompany().getUuid());
		if (company == null) {
			company = new Company();
			company.setName(subscriptionCreateEvent.getPayload().getCompany().getName());
			company.setUuid(subscriptionCreateEvent.getPayload().getCompany().getUuid());
			company.setWebsite(subscriptionCreateEvent.getPayload().getCompany().getWebsite());
		}

		// TODO add user roles
		// roleService.getRoleByRoleName(subscriptionCreateEvent.getPayload().getCompany().getName()))

		Users user = new Users();
		user.setAppDirectUserId(subscriptionCreateEvent.getUser().getUuid());
		user.setEmail(subscriptionCreateEvent.getUser().getEmail());
		user.setCreateDate(new Date());
		user.setFirstName(subscriptionCreateEvent.getUser().getFirstName());
		user.setLastName(subscriptionCreateEvent.getUser().getLastName());
		user.setCompany(company);

		return userService.saveUser(user);

	}

	private SubscriptionCreateIntegrationResponse buildSubscriptionCreateEventResponse(
			SubscriptionCreateEvent subscriptionCreateEvent) {

		Users user = userService.getUserByAppDirectId(subscriptionCreateEvent.getUser().getUuid());
		if (user == null) {
			LOGGER.info("User with appdirect id " + subscriptionCreateEvent.getUser().getUuid()
					+ " not found in database. creating a new user");
			user = createNewUserWithCompany(subscriptionCreateEvent);
		}

		Subscription subscription = subscriptionService
				.getSubscriptionByEdition(subscriptionCreateEvent.getPayload().getSubscriptionOrder().getEditionCode());
		if (subscription == null) {
			LOGGER.info("No matching subscription with name "
					+ subscriptionCreateEvent.getPayload().getSubscriptionOrder().getEditionCode()
					+ "found. Creating a new subscription");
			subscription = new Subscription();
			subscription.setEdition(subscriptionCreateEvent.getPayload().getSubscriptionOrder().getEditionCode());
			subscription.setPricingDuration(
					subscriptionCreateEvent.getPayload().getSubscriptionOrder().getPricingDuration());
		}

		Account account = new Account();
		account.setAccountIdentifier(UUID.randomUUID().toString());
		account.setStatus(AccountStatus.ACTIVE);
		account.setUsers(user);
		account.setSubscription(subscription);
		accountService.saveAccount(account);
		
		LOGGER.info("account with id " + account.getAccountIdentifier() + " created for user " + user.getAppDirectUserId()
				+ " for subscription code " + subscription.getEdition());

		SubscriptionCreateIntegrationResponse response = new SubscriptionCreateIntegrationResponse();
		response.setAccountIdentifier(account.getAccountIdentifier());
		response.setIs_success(true);
		response.setMessage("Successfully created the account!!");

		return response;
	}

}
