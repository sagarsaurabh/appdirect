package com.budgetapp.service;

import java.util.List;

import com.budgetapp.dto.ExpenseDto;
import com.budgetapp.exception.BATechnicalException;

public interface UserExpenseService {
      public List<ExpenseDto> getUserExpenses(String userId);
      
      public void saveUserExpense(ExpenseDto expense) throws BATechnicalException;
	List<ExpenseDto> getUserExpenseOfType(String userId, String budgetType);
}
