package com.budgetapp.service;

import com.budgetapp.exception.BATechnicalException;
import com.budgetapp.reports.ReportType;
import com.budgetapp.respose.ReportResponse;

public interface ReportService {
      public ReportResponse getUserExpenditureResponse(String userId, ReportType reportType) throws BATechnicalException;
}
