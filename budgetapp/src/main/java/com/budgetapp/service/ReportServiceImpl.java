package com.budgetapp.service;

import org.springframework.stereotype.Service;

import com.budgetapp.exception.BATechnicalException;
import com.budgetapp.reports.ReportProcessor;
import com.budgetapp.reports.ReportProcessorFactory;
import com.budgetapp.reports.ReportType;
import com.budgetapp.respose.ReportResponse;

@Service
public class ReportServiceImpl implements ReportService {

	@Override
	public ReportResponse getUserExpenditureResponse(String userId, ReportType reportType) throws BATechnicalException {
		ReportProcessor reportProcessor = ReportProcessorFactory.getReportProcessor(reportType);
		return reportProcessor.getReport(userId);
	}

}
