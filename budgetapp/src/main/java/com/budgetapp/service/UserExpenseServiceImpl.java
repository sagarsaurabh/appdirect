package com.budgetapp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budgetapp.dto.ExpenseDto;
import com.budgetapp.exception.BATechnicalException;
import com.budgetapp.model.BudgetType;
import com.budgetapp.model.UserExpense;
import com.budgetapp.model.Users;
import com.budgetapp.repository.UserExpenseRepository;

@Service
public class UserExpenseServiceImpl implements UserExpenseService{

	@Autowired
	private UserExpenseRepository userExpenseRepo;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private BudgetService budgetService;
	
	@Override
	public List<ExpenseDto> getUserExpenses(String userId) {
		return getExpenseDto(userExpenseRepo.getUserExpenses(userId));
	}

	
	@Override
	public List<ExpenseDto> getUserExpenseOfType(String userId, String budgetType) {
		return getExpenseDto(userExpenseRepo.getUserExpenseOfType(budgetType, userId));
	}
	
	private List<ExpenseDto> getExpenseDto(List<UserExpense> userExpenses) {
		List<ExpenseDto> expenses = new ArrayList<>();
		for(UserExpense ue : userExpenses) {
			ExpenseDto ed = new ExpenseDto();
			ed.setAmount(ue.getAmount());
			ed.setBudgetTypeName(ue.getBudgetType().getBudgetName().trim());
			ed.setUserId(ue.getUser().getAppDirectUserId().trim());
			expenses.add(ed);
		}
		return expenses;
	}


	@Override
	public void saveUserExpense(ExpenseDto expense) throws BATechnicalException {
		Users user = userService.getUserByAppDirectId(expense.getUserId());
		if(user == null) {
			throw new BATechnicalException("No such user found for appdirect userid " + expense.getUserId());
		}
		BudgetType budget = budgetService.getBudgetTypeByName(expense.getBudgetTypeName());
		if(budget == null) {
			throw new BATechnicalException("No budget type found with budgetType name " + expense.getBudgetTypeName());
		}
		
		UserExpense userExpense = new UserExpense();
		userExpense.setAmount(expense.getAmount());
		userExpense.setBudgetType(budget);
		userExpense.setUser(user);
		userExpenseRepo.save(userExpense);
		
	}


}
