package com.budgetapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budgetapp.model.Subscription;
import com.budgetapp.repository.SubscriptionRepository;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

	@Autowired
	private SubscriptionRepository subscriptionRepository;
	
	@Override
	public Subscription getSubscriptionByEdition(String edition) {
		
		return subscriptionRepository.getSubscriptionByEdition(edition);
	}

	@Override
	public Subscription saveSubscription(Subscription subscription) {
		
		return subscriptionRepository.save(subscription);
	}

}
