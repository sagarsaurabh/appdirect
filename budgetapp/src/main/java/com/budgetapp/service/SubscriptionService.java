package com.budgetapp.service;

import com.budgetapp.model.Subscription;

public interface SubscriptionService {
     public Subscription getSubscriptionByEdition(String name);
     public Subscription saveSubscription(Subscription subscription);
}
