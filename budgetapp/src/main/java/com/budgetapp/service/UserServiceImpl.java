package com.budgetapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budgetapp.model.Users;
import com.budgetapp.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepo;
	
	@Override
	public Users getUserById(long id) {
		return userRepo.findOne(id);
	}

	@Override
	public Users getUserByAppDirectId(String id) {
		return userRepo.getUsersByAppDirectUserId(id);
	}

	@Override
	public List<Users> getAllUsers() {
		 return userRepo.findAll();
	}

	@Override
	public Users saveUser(Users user) {
		
		return userRepo.save(user);
	}

}
