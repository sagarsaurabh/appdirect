package com.budgetapp.service;

import com.budgetapp.model.Role;

public interface RoleService {
    Role getRoleByRoleName(String roleName);
}
