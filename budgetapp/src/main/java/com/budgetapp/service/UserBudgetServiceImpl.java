package com.budgetapp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budgetapp.dto.UserBudgetDto;
import com.budgetapp.exception.BATechnicalException;
import com.budgetapp.model.BudgetType;
import com.budgetapp.model.UserBudget;
import com.budgetapp.model.Users;
import com.budgetapp.repository.BudgetTypeRepository;
import com.budgetapp.repository.UserBudgetRepositoty;

@Service
public class UserBudgetServiceImpl implements UserBudgetService{

	@Autowired
	private UserBudgetRepositoty userBudgetRepo;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private BudgetTypeRepository budgetRepository;
	
	
	
	@Override
	public void addUserBudget(UserBudgetDto userBudgetRequest) throws BATechnicalException {
		String userId = userBudgetRequest.getAppdirectUserId();
		Users user = userService.getUserByAppDirectId(userId);
		if(user == null) {
			throw new BATechnicalException("User not registered");
		}
	    BudgetType budgetType = budgetRepository.getByBudgetName(userBudgetRequest.getBudgetType());
	    UserBudget ub = new  UserBudget();
	    ub.setUser(user);
	    ub.setBudget(budgetType);
	    ub.setBudgetAmount(userBudgetRequest.getBudgetAmount());
	    ub.setFromDate(new Date());
	    userBudgetRepo.save(ub);
	}

	@Override
	public List<UserBudgetDto> getUserBudgetList(String userId) {
		
		return getUserBudgetDto(userBudgetRepo.getBudgetsByUserId(userId));
		
	}

	private List<UserBudgetDto> getUserBudgetDto(List<UserBudget> budgetsByUserId) {
		List<UserBudgetDto> userbudgets = new ArrayList<>();
		for(UserBudget ub : budgetsByUserId) {
			UserBudgetDto budgetDto = new UserBudgetDto();
			budgetDto.setAppdirectUserId(ub.getUser().getAppDirectUserId());
			budgetDto.setBudgetAmount(ub.getBudgetAmount());
			budgetDto.setBudgetType(ub.getBudget().getBudgetName());
			budgetDto.setFromDate(ub.getFromDate());
			userbudgets.add(budgetDto);
		}
		return userbudgets;
	}

}
