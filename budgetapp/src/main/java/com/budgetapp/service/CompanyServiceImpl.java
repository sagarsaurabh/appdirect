package com.budgetapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budgetapp.model.Company;
import com.budgetapp.repository.CompanyRepository;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	private CompanyRepository companyRepository;
	
	@Override
	public Company getCompanyByUuId(String uuid) {
		
		return companyRepository.getCompanyByUuid(uuid);
	}

}
