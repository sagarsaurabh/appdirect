package com.budgetapp.service;

import com.budgetapp.model.Company;

public interface CompanyService {
    public Company getCompanyByUuId(String uuid);
}
