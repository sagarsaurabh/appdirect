package com.budgetapp.service;

import java.util.List;

import com.budgetapp.model.BudgetType;

public interface BudgetService {
    
	public List<BudgetType> getAllBudgetTypes();
	public BudgetType getBudgetByBudgetId(Long id);
	public BudgetType getBudgetTypeByName(String budgetName);
	
}
