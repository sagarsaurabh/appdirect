package com.budgetapp.service;

import com.budgetapp.exception.BATechnicalException;
import com.budgetapp.respose.integration.SubscriptionCancelIntegrationResponse;
import com.budgetapp.respose.integration.SubscriptionChangeIntegrationResponse;
import com.budgetapp.respose.integration.SubscriptionCreateIntegrationResponse;

public interface SubscriptionActionService {

	SubscriptionCreateIntegrationResponse createSubscription(String url) throws BATechnicalException;

	SubscriptionCancelIntegrationResponse cancelSubscription(String url) throws BATechnicalException;

	SubscriptionChangeIntegrationResponse changeSubscription(String url) throws BATechnicalException;
     
    	
}
