package com.budgetapp.service;

import com.budgetapp.model.Account;

public interface AccountService {
      public Account saveAccount(Account account);
      public Account getAccountByAccountIdentifier(String accountIdentifier);
}
