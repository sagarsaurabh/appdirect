package com.budgetapp.service;

import java.util.List;

import com.budgetapp.dto.UserBudgetDto;
import com.budgetapp.exception.BATechnicalException;

public interface UserBudgetService {
	public void addUserBudget(UserBudgetDto userBudgetRequest) throws BATechnicalException;

	List<UserBudgetDto> getUserBudgetList(String userId);
}
