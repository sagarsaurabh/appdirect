package com.budgetapp.service;

import java.util.List;

import com.budgetapp.model.Users;

public interface UserService {
	 /*
	  * getUserById(long id)
	  */
     public Users getUserById(long id);
     
     public Users getUserByAppDirectId(String id);
     
     public List<Users> getAllUsers();
    
	 public Users saveUser(Users user);
}
