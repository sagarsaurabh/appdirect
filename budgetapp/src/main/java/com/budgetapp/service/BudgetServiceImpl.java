package com.budgetapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.budgetapp.model.BudgetType;
import com.budgetapp.repository.BudgetTypeRepository;

@Service
public class BudgetServiceImpl implements BudgetService {

	@Autowired
	private BudgetTypeRepository budgetTypeRepo;
	
	@Override
	public List<BudgetType> getAllBudgetTypes() {
		
		return budgetTypeRepo.findAll();
	}

	@Override
	public BudgetType getBudgetByBudgetId(Long id) {
		return budgetTypeRepo.getOne(id);
		
	}

	@Override
	public BudgetType getBudgetTypeByName(String budgetName) {
		return budgetTypeRepo.getByBudgetName(budgetName);
	}

}
