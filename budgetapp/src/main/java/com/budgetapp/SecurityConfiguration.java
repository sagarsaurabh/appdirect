package com.budgetapp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import com.budgetapp.repository.UserRepository;
import com.budgetapp.security.CustomUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserRepository userRepository;
	
	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	    	http
            .csrf().disable();
	    	http
	            .authorizeRequests()
	                .antMatchers("/resources/**", "/integration/**", "/user/**").permitAll()
	                .anyRequest().authenticated()
	                .and()
	            .openidLogin()
	                .loginPage("/login")
	                .permitAll()
	                .authenticationUserDetailsService(new CustomUserDetailsService(userRepository))
	                .attributeExchange("https://www.appdirect.com.*")
	                    .attribute("email")
	                        .type("http://axschema.org/contact/email")
	                        .required(true)
	                        .and()
	                    .attribute("firstname")
	                        .type("http://axschema.org/namePerson/first")
	                        .required(true)
	                        .and()
	                    .attribute("lastname")
	                        .type("http://axschema.org/namePerson/last")
	                        .required(true);
	                 
	    }
}