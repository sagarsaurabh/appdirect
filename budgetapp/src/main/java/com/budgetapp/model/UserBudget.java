package com.budgetapp.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_budget")
public class UserBudget {

	private long userBudgetId;
	private Users user;
	private BudgetType budget;
	private Date fromDate;
	private Date thruDate;
	private BigDecimal budgetAmount;
	
	
	@Column(name="budget_amount")
	public BigDecimal getBudgetAmount() {
		return budgetAmount;
	}
	
	public void setBudgetAmount(BigDecimal budgetAmount) {
		this.budgetAmount = budgetAmount;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_budget_id")
	public long getUserBudgetId() {
		return userBudgetId;
	}
	public void setUserBudgetId(long userBudgetId) {
		this.userBudgetId = userBudgetId;
	}
	
	
	@ManyToOne
	@JoinColumn(name="user_id")
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	
	@ManyToOne
	@JoinColumn(name="budget_id")
	public BudgetType getBudget() {
		return budget;
	}
	
	public void setBudget(BudgetType budget) {
		this.budget = budget;
	}
	@Column(name="from_date")
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	@Column(name="thru_date")
	public Date getThruDate() {
		return thruDate;
	}
	public void setThruDate(Date thruDate) {
		this.thruDate = thruDate;
	}
	
}
