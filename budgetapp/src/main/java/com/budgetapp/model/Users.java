package com.budgetapp.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Table
@Entity(name="users")
public class Users {
	
	private long userId;
	
	private String email;
	private String appDirectUserId;
	private String firstName;			
	private String lastName;			
	private Date createDate;			
	private boolean isDeleted;
    private Company company;	
    private List<Account> accounts;
	private List<Role> roles;
    private List<UserExpense> expenses;
    
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	@Column(name="first_name")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name="last_name")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name="createDate")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	@Column(name="is_deleted")
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}
	@Column(name="appdirect_user_id")
	public String getAppDirectUserId() {
		return appDirectUserId;
	}
	public void setAppDirectUserId(String appDirectUserId) {
		this.appDirectUserId = appDirectUserId;
	}
	
	@Column(name="email")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="company_id")
	@JsonBackReference
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	@OneToMany(fetch = FetchType.EAGER, mappedBy="users")
	@JsonManagedReference
	public List<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(List<Account> accounts) {
		this.accounts = accounts;
	}
	
	@ManyToMany(cascade = CascadeType.ALL)  
    @JoinTable(name="users_role", 
      joinColumns = {@JoinColumn(name = "users_id")},
      inverseJoinColumns = {@JoinColumn(name = "role_id")})
	@JsonBackReference
	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	
	@OneToMany(cascade={CascadeType.ALL}, mappedBy="user")
	public List<UserExpense> getExpenses() {
		return expenses;
	}

	public void setExpenses(List<UserExpense> expenses) {
		this.expenses = expenses;
	}

	@Override
	public String toString() {
		return "Users [userId=" + userId + ", email=" + email + ", appDirectUserId=" + appDirectUserId + ", firstName="
				+ firstName + ", lastName=" + lastName + ", createDate=" + createDate + ", isDeleted=" + isDeleted
				+ ", company=" + company + ", accounts=" + accounts +"]";
	}
	
}
