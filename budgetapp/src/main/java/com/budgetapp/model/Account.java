package com.budgetapp.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.budgetapp.dto.AccountStatus;
import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name="account")
public class Account {

	private String accountIdentifier;

	private AccountStatus status;
    
	private long account_id;
	
	private Users users;
	
	private Subscription subscription; 

	@Id
	@Column(name="account_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public long getAccount_id() {
		return account_id;
	}

	public void setAccount_id(long account_id) {
		this.account_id = account_id;
	}
	
	@ManyToOne( cascade = {CascadeType.ALL}, fetch=FetchType.EAGER )
	@JoinTable(name="user_account", joinColumns={@JoinColumn(name ="account_id")},
	inverseJoinColumns={@JoinColumn(name ="user_id")}
    )
	@JsonBackReference
	public Users getUsers() {
		return users;
	}
    
	public void setUsers(Users users) {
		this.users = users;
	}

	@Column(name="account_identifier")
	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}

	@Column(name="status")
	@Enumerated(EnumType.STRING)
	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="subscription_id")
	public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}
	
}
