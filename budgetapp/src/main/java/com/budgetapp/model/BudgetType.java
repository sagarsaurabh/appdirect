package com.budgetapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "budget")
public class BudgetType {
	private long budgetId;
	private String budgetName;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="budget_id")
	public long getBudgetId() {
		return budgetId;
	}

	public void setBudgetId(long budgetId) {
		this.budgetId = budgetId;
	}

	@Column(name = "Budget_name")
	public String getBudgetName() {
		return budgetName;
	}

	public void setBudgetName(String budgetName) {
		this.budgetName = budgetName;
	}

	@Override
	public String toString() {
		return "BudgetType [budgetId=" + budgetId + ", budgetName=" + budgetName + "]";
	}
	
}
