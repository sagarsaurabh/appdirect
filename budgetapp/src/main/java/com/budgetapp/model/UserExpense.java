package com.budgetapp.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="user_expense")
public class UserExpense {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_expense_id")
	private long userExpenseId;
	
	@Column(name="amount")
      private BigDecimal amount;
	  
	
	@ManyToOne(cascade= {CascadeType.ALL})
	@JoinColumn(name = "budget_type_id")
	private BudgetType budgetType;
	
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="user_id")
	private Users user;

	public long getUserExpenseId() {
		return userExpenseId;
	}

	public void setUserExpenseId(long userExpenseId) {
		this.userExpenseId = userExpenseId;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BudgetType getBudgetType() {
		return budgetType;
	}

	public void setBudgetType(BudgetType budgetType) {
		this.budgetType = budgetType;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}
	
	
	
}
