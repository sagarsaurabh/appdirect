package com.budgetapp.reports;

import com.budgetapp.exception.BATechnicalException;
import com.budgetapp.respose.ReportResponse;

public interface ReportProcessor {
      public ReportResponse getReport(String userId) throws BATechnicalException;
}
