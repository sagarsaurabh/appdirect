package com.budgetapp.reports;

import com.budgetapp.exception.BATechnicalException;

public class ReportProcessorFactory {
       
	public static ReportProcessor getReportProcessor(ReportType reportType) throws BATechnicalException {
		
		switch (reportType) {
		case MONTHLY:
			return new MonthlyReportProcessor();
		case QUATERLY:
			return new QuarterlyReportProcessor();
		case ANNUAL:
			return new AnnualReportProcessor();

		default:
			throw new BATechnicalException("report of typr " + reportType.toString() + " not supported as of now");
		}
		
	}
	
}
