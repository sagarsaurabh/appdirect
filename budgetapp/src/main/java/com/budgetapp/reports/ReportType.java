package com.budgetapp.reports;

public enum ReportType {
      MONTHLY("monthly"), QUATERLY("quaterly"), ANNUAL("annual");
	private String type;

	private ReportType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}
	  
	public static ReportType fromRequestParameter(String reportType) {
	    if (reportType != null) {
	      for (ReportType rt : ReportType.values()) {
	        if (reportType.equalsIgnoreCase(rt.type)) {
	          return rt;
	        }
	      }
	    }
	    return null;
	  }
      
}