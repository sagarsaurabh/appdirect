package com.budgetapp.respose;

import java.math.BigDecimal;

import com.budgetapp.model.BudgetType;
import com.fasterxml.jackson.annotation.JsonProperty;


public class UserBudgetResponse extends RestResponse{
	
	@JsonProperty("userBudgetId")
	private long userBudgetId;
	
	@JsonProperty("budgetType")
	private BudgetType budget;
	
	@JsonProperty("fromMonth")
	private String fromMonth;
	
	@JsonProperty("fromMonth")
	private String toMonth;
	
	@JsonProperty("budgetAmount")
	private BigDecimal budgetAmount;

	public long getUserBudgetId() {
		return userBudgetId;
	}

	public void setUserBudgetId(long userBudgetId) {
		this.userBudgetId = userBudgetId;
	}

	public BudgetType getBudget() {
		return budget;
	}

	public void setBudget(BudgetType budget) {
		this.budget = budget;
	}

	public String getFromMonth() {
		return fromMonth;
	}

	public void setFromMonth(String fromMonth) {
		this.fromMonth = fromMonth;
	}

	public String getToMonth() {
		return toMonth;
	}

	public void setToMonth(String toMonth) {
		this.toMonth = toMonth;
	}

	public BigDecimal getBudgetAmount() {
		return budgetAmount;
	}

	public void setBudgetAmount(BigDecimal budgetAmount) {
		this.budgetAmount = budgetAmount;
	}

}
