package com.budgetapp.respose;

import java.util.Date;

public class UserResponse extends RestResponse{
	private String userName;
	private String email;
	private String name;
	private String appDirectUserId;
	private String firstName;			
	private String lastName;			
	private Date createDate;			
	private boolean isDeleted;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAppDirectUserId() {
		return appDirectUserId;
	}
	public void setAppDirectUserId(String appDirectUserId) {
		this.appDirectUserId = appDirectUserId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public boolean isDeleted() {
		return isDeleted;
	}
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	
	
}
