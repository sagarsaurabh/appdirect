package com.budgetapp.respose.integration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubscriptionCreateIntegrationResponse extends IntegrationResponse{

	@JsonProperty("accountIdentifier")
	private String accountIdentifier;

	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}
		
}
