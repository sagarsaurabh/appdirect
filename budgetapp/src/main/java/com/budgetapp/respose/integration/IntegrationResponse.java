package com.budgetapp.respose.integration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IntegrationResponse {
	
    @JsonProperty("success")
	protected boolean is_success;
    
    @JsonProperty("message")
    protected String message;

	public boolean isIs_success() {
		return is_success;
	}

	public void setIs_success(boolean is_success) {
		this.is_success = is_success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
    
    
}
