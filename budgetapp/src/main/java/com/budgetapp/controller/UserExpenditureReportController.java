package com.budgetapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.budgetapp.exception.BATechnicalException;
import com.budgetapp.reports.ReportType;
import com.budgetapp.respose.ReportResponse;
import com.budgetapp.service.ReportService;


/**
 * @author Saurabh
 *
 */
@RestController
public class UserExpenditureReportController {
      
	@Autowired
	private ReportService reportService;
	
	
	
	/**
	 * @param userId
	 * @param reportType
	 * @return
	 * @throws BATechnicalException
	 * 
	 * This takes reporttype as query parameter to process differnt report accordingly
	 */
	@RequestMapping(value="report/user/{userid}/expenses/")
	public ReportResponse getUserExpenditureResponse(@PathVariable("userid") String userId, @RequestParam("reporttype") String reportType) throws BATechnicalException {
		return reportService.getUserExpenditureResponse(userId, ReportType.fromRequestParameter(reportType));
	}
	
	
}
