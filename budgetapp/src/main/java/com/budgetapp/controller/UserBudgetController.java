package com.budgetapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.budgetapp.dto.ExpenseDto;
import com.budgetapp.dto.UserBudgetDto;
import com.budgetapp.exception.BATechnicalException;
import com.budgetapp.respose.RestResponse;
import com.budgetapp.service.UserBudgetService;
import com.budgetapp.service.UserExpenseService;

@RestController
public class UserBudgetController {

	@Autowired
	private UserBudgetService userBudgetService;
	
	@Autowired
	private UserExpenseService userExpenseService;
	
	
	@RequestMapping(value = "/user/budget" , method = RequestMethod.POST)
	public RestResponse saveUserBudget(@RequestBody UserBudgetDto budgetRequest) throws BATechnicalException {
		userBudgetService.addUserBudget(budgetRequest);
		RestResponse response = new RestResponse();
		response.setMessgae("budget of type "+ budgetRequest.getBudgetType() +" successfully created");
		response.setStatus(HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/user/{id}/budget" , method = RequestMethod.GET)
	public List<UserBudgetDto> getUserBudget(Authentication auth, @PathVariable("id") String userId) {
		List<UserBudgetDto> budgets = userBudgetService.getUserBudgetList(userId);
		return budgets;
		
	}
	
	@RequestMapping(value = "/user/expense" , method = RequestMethod.POST)
    public RestResponse addUserExpense(@RequestBody ExpenseDto expense) throws BATechnicalException {
		userExpenseService.saveUserExpense(expense);
		RestResponse response = new RestResponse();
		response.setMessgae("Expense of type "+ expense.getBudgetTypeName() +" with amount " + expense.getAmount() +" successfully created");
		response.setStatus(HttpStatus.OK);
		return response;
    }
	
	@RequestMapping(value="/user/{id}/expenses", method=RequestMethod.GET)
	public List<ExpenseDto> getUserExpenses(@PathVariable("id") String userId) {
		return userExpenseService.getUserExpenses(userId);
	}
	
	@RequestMapping(value="/user/{id}/budget/{budgetname}/expenses", method=RequestMethod.GET)
	public List<ExpenseDto> getUserExpensesOfType(@PathVariable("id") String userId, @PathVariable("budgetname") String budgetName) {
		return userExpenseService.getUserExpenseOfType(userId, budgetName);
	}
	
	
}
