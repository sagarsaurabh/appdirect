package com.budgetapp.controller.eventcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.budgetapp.exception.BATechnicalException;
import com.budgetapp.respose.integration.SubscriptionCancelIntegrationResponse;
import com.budgetapp.respose.integration.SubscriptionChangeIntegrationResponse;
import com.budgetapp.respose.integration.SubscriptionCreateIntegrationResponse;
import com.budgetapp.service.SubscriptionActionService;

@RestController
@RequestMapping(path="/integration/subscription")
public class SubscriptionEventController {
	
	@Autowired
	private SubscriptionActionService subscriptionService;
	
	
	@RequestMapping(path="/create", method=RequestMethod.GET)
	public SubscriptionCreateIntegrationResponse createSubscription(@RequestParam("eventUrl") String url) throws BATechnicalException {
        return subscriptionService.createSubscription(url);     		
		
	}
	
	@RequestMapping(path="/cancel", method=RequestMethod.GET)
	public SubscriptionCancelIntegrationResponse cancelSubscription(@RequestParam("eventUrl") String url) throws BATechnicalException {
        return subscriptionService.cancelSubscription(url);     		
	}
	
	@RequestMapping(path="/change", method=RequestMethod.GET)
	public SubscriptionChangeIntegrationResponse changeSubscription(@RequestParam("eventUrl") String url) throws BATechnicalException {
        return subscriptionService.changeSubscription(url);     		
	}
	
}
