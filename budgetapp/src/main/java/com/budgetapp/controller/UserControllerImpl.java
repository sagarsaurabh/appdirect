package com.budgetapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.budgetapp.model.Users;
import com.budgetapp.service.UserService;

@RestController

public class UserControllerImpl{

	@Autowired
	private UserService userService;
	

	@RequestMapping(value="/users")
	public List<Users> getAllUsers() {
		return userService.getAllUsers();
		
		
	}
	
}
