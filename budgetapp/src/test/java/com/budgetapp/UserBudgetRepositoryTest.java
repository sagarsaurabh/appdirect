package com.budgetapp;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.budgetapp.model.BudgetType;
import com.budgetapp.model.UserBudget;
import com.budgetapp.model.Users;
import com.budgetapp.repository.BudgetTypeRepository;
import com.budgetapp.repository.UserBudgetRepositoty;
import com.budgetapp.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserBudgetRepositoryTest {

	@Autowired
	private UserBudgetRepositoty userbudgerRepo;
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	
	private BudgetTypeRepository budgetTypeRepo;
	
	@Test
	@Rollback(false)
	public void saveUserBudgetTest() {
		Users user = new Users();
		//user.setName("saurabhs");
		user.setEmail("saursabhascom");
		
		userRepo.save(user);
	    BudgetType budgetType = budgetTypeRepo.findOne(1L);
	    UserBudget ub = new  UserBudget();
	    ub.setUser(user);
	    ub.setBudget(budgetType);
	    ub.setBudgetAmount(new BigDecimal(100));
	    ub.setFromDate(new Date());
	    userbudgerRepo.save(ub);
	}
	
	
}
