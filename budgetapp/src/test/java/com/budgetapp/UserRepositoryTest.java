package com.budgetapp;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.budgetapp.model.Users;
import com.budgetapp.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@DataJpaTest
public class UserRepositoryTest {

	@Autowired
	private UserRepository userRepo;
	
	@Test
	public void testGetCustomUser() {
		Users user = userRepo.getUsersByAppDirectUserId("d11a25aa-19f9-4192-986d-a6802e659d27");
		assertNotNull(user);
	}

}
