package com.budgetapp;

import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.budgetapp.model.BudgetType;
import com.budgetapp.repository.BudgetTypeRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BudgetappApplicationTests {

	@Autowired
	private BudgetTypeRepository budgetTypeRepo;
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void getAllBudgetTypes() {
		List<BudgetType> budgetTypes = budgetTypeRepo.findAll();
		assertNotNull(budgetTypes);
	}

}
